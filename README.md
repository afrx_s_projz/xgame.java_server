
# xgame.java_server

----

### 这有什么意思？
- 如果游戏只是一段服务器端代码，那有什么意思？这并不能表达我的所有想法！
- 有时候我甚至极度反感程序员这个____；
- 我更喜欢美术，艺术与代码结合在一起那才是游戏！
- 欢迎你来到童话般的迷宫世界，我将在那里等着你；
- https://store.steampowered.com/app/2865300/_/
- https://space.bilibili.com/492205856/

### 项目介绍
- 该项目以前叫 xgame-code_server，是一个基于 Java 13 实现的游戏服务器框架；
- 一切代码都做了简化，服务器只分为业务服务器和网关服务器；
- 业务服务器会通过 ServerJobType 区分具体功能；
- 集群和配置服务使用了 Nacos；
- 消息通信部分使用 Google Protobuf；
- 删除了对 Excel 文档的支持，直接使用 JSON 或 CSV 文件；
- 如果想对照老版本代码，可以切换到 achive/2021_old_ver 这个分支上；

### 软件架构
- BizServer 业务服务器，所有的游戏逻辑都在这里实现；
- GatewayServer 网关服务器，维护客户端连接；
- Comm 一些通用代码，支撑上面两个服务器；
