package org.xgame.bizserver.module.user.db;

import com.alibaba.fastjson.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.xgame.bizserver.base.MongoClientSingleton;
import org.xgame.bizserver.module.user.model.UserModel;

public class UserDAOTest {
    @Before
    public void init() {
        JSONObject joDBConfig = new JSONObject();
        joDBConfig.put("mongoDBConnStr", "mongodb://127.0.0.1:27017/xxoo?retryWrites=true&w=majority&maxPoolSize=32");
        JSONObject joRootConfig = new JSONObject();
        joRootConfig.put("dbConfig", joDBConfig);

        MongoClientSingleton.getInstance().init(joRootConfig);
    }

    @Test
    public void test_getUserByGuestCode() {
        UserModel userM = (new UserDAO()).getUserByGuestCode("123");
        Assert.assertNotNull(userM);

        userM.setUserName("Haijiang");
        userM.setLastLoginTime(System.currentTimeMillis());
        (new UserDAO()).saveOrUpdate(userM);
    }
}
