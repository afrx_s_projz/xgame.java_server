package org.xgame.bizserver.module.item;

interface IServ_onServerShutdown {
    /**
     * 当服务器停机
     */
    default void onServerShutdown() {
    }
}
