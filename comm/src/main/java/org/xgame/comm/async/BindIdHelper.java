package org.xgame.comm.async;

/**
 * 绑定 Id 助手, 在这里计算出长整型的绑定 Id.
 * 这里没有复杂的 HashCode 算法,
 * 只要保证相同字符串每次计算得到的长整型数值相同即可...
 * 算法核心就是简单的相加每个字符串的 ASCII 码的数值
 */
class BindIdHelper {
    /**
     * 私有化类默认构造器
     */
    private BindIdHelper() {
    }

    /**
     * 获取长整数值
     *
     * @param strVal 字符串值
     * @return 长整数值
     */
    public static long getLongVal(String strVal) {
        if (null == strVal ||
            strVal.isEmpty()) {
            return 0;
        }

        long bindId = 0;

        for (int i = 0; i < strVal.length(); i++) {
            bindId += strVal.charAt(i);
        }

        return bindId;
    }
}
